#!/usr/bin/env python3

"""
IX-API Test Suite Management

This file provides the entry point for
the integration test suite application.

Having a single entry point has been quite
useful with docker in the past.
"""

import sys
import inspect

from cli.utils import loaders
from cli.utils.text import print_f, print_doc


def main():
    """IX-API  entry point"""
    # Get all scripts
    scripts = loaders.load_package_path("cli/scripts")

    try:
        script_name = sys.argv[1]
    except IndexError:
        print_header()
        print_usage(scripts)
        return

    # Use script provided in argv[1]
    try:
        script = scripts[script_name]
    except KeyError:
        print_header()
        print_f("<red><b>Error:</b> Unknown script.</red>")
        return

    # Replace script in argv and invoke main
    sys.argv = [script_name, *sys.argv[2:]]
    script.__main__()


def print_header():
    """Print IX Api banner"""
    print_f(
        "<green><b>IX-API Test Suite</b></green>"
        "                              "
        "v.0.1.0"
    )
    print_f()


def print_usage(scripts):
    """Print usage instructions"""
    # Show a list of available scripts
    print_doc("""
        <b>Usage:</b> manage.py &lt;script&gt;

        <b>Available scripts:</b>
    """)
    scripts = list(scripts.items())
    scripts = sorted(scripts, key=lambda s: s[0])
    for name, module in scripts:
        try:
            doc = inspect.cleandoc(module.__main__.__doc__)
        except:
            doc = ""

        print_f("    <b>{}</b>", name)
        for l in doc.split("\n"):
            print_f("        {}", l)
    print_f()


if __name__ == "__main__":
    main()
