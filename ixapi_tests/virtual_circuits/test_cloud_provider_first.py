"""
P2P VC NetworkService Tests
"""

import secrets
from pprint import pprint

import pytest
from ixapi_client.v2.resources import (
    catalog,
    config,
    service,
    crm,
)

from cli.utils.text import print_f, print_err
from ixapi_tests.utils.state import await_state



@pytest.mark.reset
def test_configure_provider_first_cloud_vc(session, args):
    """Create a cloud network service and provide a cloud key"""
    account = args.account
    subaccount = args.subaccounts[0]

    print_f("")
    print_f("<b>creating and configuring a provider "
            "first cloud network service</b>")

    # Retrieve product offering based on the provider key
    ret, offering = catalog.product_offerings_retrieve(
        session,
        args.cloud_workflow_exchange_first_product_offering_id)

    cloud_key = args.cloud_workflow_provider_first_product_offering_cloud_key

    # Use product offering from fixture to create the service
    ret, offerings = catalog.product_offerings_list(session, filters={
        "cloud_key": cloud_key,
    })
    ret.raise_for_status()
    assert offerings, "no offerings were found for the cloud_key"

    offering = offerings[0]
    diversity = offering["diversity"]

    print_f("<cyan>product-offering:</cyan> <orange>{}</orange> ID: {}",
            offering["name"],
            offering["id"])
    print_f("<b><orange>{}</orange></b>",
            offering["display_name"])
    network_service = create_network_service(session, args, offering)

    print_f("<orange>Required diversity:</orange> {}", diversity)

    for idx in range(1, diversity+1):
        conf = create_network_service_config(
            session, args, network_service, idx)

        # Refresh network service
        _, network_service = service.network_services_retrieve(
            session, network_service["id"])

        print_f("<b>network service state:</b> {}", network_service["state"])
        pprint(network_service["status"])


    # Wait for the network service to become productive
    network_service = await_state(
        session,
        service.network_services_retrieve,
        network_service["id"],
        "production")

    print_f("<green><b>cloud</b> virtual circuit: configured.</green>")
    pprint(network_service)


def create_network_service(session, args, offering):
    """Create network service"""
    account = args.account
    subaccount = args.subaccounts[0]
    # We need to use the cloud key provided for us
    cloud_key = args.cloud_workflow_provider_first_product_offering_cloud_key
    ret, svc = service.network_services_create(session, {
        "type": "cloud_vc",
        "product_offering": offering["id"],
        "managing_account": account["id"],
        "billing_account": account["id"],
        "consuming_account": subaccount["id"],
        "cloud_key": cloud_key,
    })

    if ret.status_code >= 400:
        print_err(svc)
        pytest.xfail("could not create network service")
    print_f("<green>Created network service</green> ID: <b>{}</b>", svc["id"])
    pprint(svc)

    return svc


def create_network_service_config(session, args, svc, handover):
    """Create config"""
    subaccount = args.subaccounts[0]
    account = args.account

    conn = args.cloud_handover_connections[handover-1]

    ret, nsc = config.network_service_configs_create(session, {
        "type": "cloud_vc",
        "network_service": svc["id"],
        "role_assignments": [
            subaccount["implementation_role_assignment_id"],
            subaccount["noc_role_assignment_id"],
        ],
        "connection": conn,
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount["id"],
        "cloud_vlan": None,
        "handover": handover,
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlan": 142,
            "vlan_ethertype": "0x8100",
        },
    })
    if ret.status_code >= 400:
        print_err(nsc)
        pytest.xfail("could not create network service configuration")

    print_f("<green>created network config</green> ID: <b>{}</b>", svc["id"])
    pprint(nsc)

    return nsc
