"""
Test P2MP VC NetworkServices
"""

from pprint import pprint

import pytest
from ixapi_client.v2.resources import (
    catalog,
    config,
    service,
    crm,
)

from cli.utils.text import print_f, print_err
from ixapi_tests.utils.state import await_state


@pytest.mark.reset
def test_configure_p2mp_network_service(session, args):
    """Configure a p2mp / etree for two peers"""

    subaccount1 = args.subaccounts[0]
    subaccount2 = args.subaccounts[1]

    print_f("")
    print_f("<b>creating and configuring a non public mp2mp network service</b>")

    # Get product offering
    ret, offering = catalog.product_offerings_retrieve(
        session, args.p2mp_product_offering_id)
    ret.raise_for_status()

    print_f("<cyan>product-offering:</cyan> <orange>{}</orange> ID: {}",
            offering["name"],
            offering["id"])

    p2mpns = create_network_service(session, args, offering)

    print_f("<grey>adding member joining rule for:</grey> subaccount 2")
    rule = create_member_joining_rule(session, args, subaccount2, p2mpns)
    pprint(rule)

    print_f("<grey>configuring network service for: "
            "</grey> peer 1, <b>root</b>, subaccount 1")
    conf_a = create_network_service_config(
        session, args, subaccount1, p2mpns, "root")
    pprint(conf_a)

    print_f("<grey>configuring network service for:</grey> "
            "peer 2, <b>leaf</b>, subaccount 2")
    conf_b = create_network_service_config(
        session, args, subaccount2, p2mpns, "leaf")
    pprint(conf_b)

    # Wait for the network service to become productive
    mp2mpns = await_state(
        session,
        service.network_services_retrieve,
        p2mpns["id"],
        "production")

    print_f("<green><b>p2mp</b> virtual circuit: configured.</green>")
    pprint(mp2mpns)



def create_network_service(session, args, offering):
    """Create network service"""
    account = args.account
    subaccount1 = args.subaccounts[0]
    subaccount2 = args.subaccounts[1]
    ret, svc = service.network_services_create(session, {
        "type": "p2mp_vc",
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount1["id"],
        "product_offering": offering["id"],
        "public": False,
        "name": "my-p2mp-etree",
    })

    if ret.status_code >= 400:
        print_err(svc)
        pytest.xfail("could not create network service")
    print_f("<green>Created network service:</green> <b>{}, ID: {}</b>",
            svc["name"], svc["id"])

    return svc


def create_network_service_config(session, args, subaccount, svc, role):
    """Create config"""
    account = args.account
    ret, nsc = config.network_service_configs_create(session, {
        "type": "p2mp_vc",
        "network_service": svc["id"],
        "role": role,
        "role_assignments": [
            subaccount["implementation_role_assignment_id"],
            subaccount["noc_role_assignment_id"],
        ],
        "connection": subaccount["connection_id"],
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount["id"],
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlan": 46,
            "vlan_ethertype": "0x8100",
        },
    })
    if ret.status_code >= 400:
        print_err(nsc)
        pytest.xfail("could not create network service configuration")

    return nsc


def create_member_joining_rule(session, args, subaccount, svc):
    """Create member joining rule"""
    account = args.account
    ret, rule = service.member_joining_rules_create(session, {
        "type": "allow",
        "managing_account": account["id"],
        "consuming_account": subaccount["id"],
        "network_service": svc["id"],
    })
    if ret.status_code >= 400:
        print_err(rule)
        pytest.xfail("could not create member joining rule")

    return rule
