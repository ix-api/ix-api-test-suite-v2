"""
P2P VC NetworkService Tests
"""

from pprint import pprint

import pytest
from ixapi_client.v2.resources import (
    catalog,
    config,
    service,
    crm,
)

from cli.utils.text import print_f, print_err
from ixapi_tests.utils.state import await_state


@pytest.mark.reset
def test_configure_p2p_network_service(session, args):
    """Configure a p2p network service"""
    subaccount1 = args.subaccounts[0]
    subaccount2 = args.subaccounts[1]

    print_f("")
    print_f("<b>creating and configuring a p2p network service</b>")

    # Get product offering
    ret, offering = catalog.product_offerings_retrieve(
        session, args.p2p_product_offering_id)
    ret.raise_for_status()

    print_f("<cyan>product-offering:</cyan> <orange>{}</orange> ID: {}",
            offering["name"],
            offering["id"])

    p2pns = create_network_service(session, args, offering)

    print_f("<grey>configuring network service for:</grey> A side, subaccount 1")
    conf_a = create_p2p_vc_nsc(session, args, subaccount1, p2pns)
    pprint(conf_a)

    print_f("<grey>configuring network service for:</grey> B side, subaccount 2")
    conf_b = create_p2p_vc_nsc(session, args, subaccount2, p2pns)
    pprint(conf_b)

    # Wait for the network service to become productive
    p2pns = await_state(
        session,
        service.network_services_retrieve,
        p2pns["id"],
        "production")

    print_f("<green>p2p virtual circuit: configured.</green>")
    pprint(p2pns)


def create_network_service(session, args, offering):
    """Create network service"""
    account = args.account
    subaccount1 = args.subaccounts[0]
    subaccount2 = args.subaccounts[1]
    print_f("creating network service: <b>p2p_vc</b>")
    print_f("   managed by: <b>account (ID: {})</b>", account["id"])
    print_f("  consumed by: <b>subaccount 1 (ID: {})</b>", subaccount1["id"])
    print_f("    joined by: <b>subaccount 2 (ID: {})</b>", subaccount2["id"])

    ret, p2pns = service.network_services_create(session, {
        "type": "p2p_vc",
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount1["id"],
        "product_offering": offering["id"],
        "joining_member_account": subaccount2["id"],
    })
    if ret.status_code >= 400:
        print_err(p2pns)
        pytest.xfail("could not create network service")

    print_f("<green>created network service:</green> p2p, ID: {}", p2pns["id"])

    return p2pns


def create_p2p_vc_nsc(s, args, subaccount, p2pns):
    """create p2p network service config"""
    account = args.account
    ret, nsc = config.network_service_configs_create(s, {
        "type": "p2p_vc",
        "network_service": p2pns["id"],
        "role_assignments": [
            subaccount["implementation_role_assignment_id"],
            subaccount["noc_role_assignment_id"],
        ],
        "connection": subaccount["connection_id"],
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount["id"],
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlan": 45,
            "vlan_ethertype": "0x8100",
        },
    })
    if ret.status_code >= 400:
        print_err(nsc)
        pytest.xfail("could not create network service config")

    return nsc
