
import time

from cli.utils.text import print_f

from pprint import pprint


def await_state(
    session,
    res,
    pk,
    state,
    tries=10,
    raise_on_error_state=True
):
    """Poll state of an object"""
    ntry = 0
    while ntry < tries:
        ntry += 1
        ret, obj = res(session, pk)
        ret.raise_for_status()

        obj_state = obj.get("state", "unknown")
        if obj_state == state:
            return obj

        if raise_on_error_state and obj_state == "error":
            raise Exception("error state reached".format(state))

        # Print current stateand status if any
        status = obj.get("status", [])
        print_f("<grey>current state:</grey> {}, <grey>status:</grey>",
            obj_state)
        pprint(status)
        print("")

        time.sleep(ntry)

    raise Exception("state {} never reached".format(state))
