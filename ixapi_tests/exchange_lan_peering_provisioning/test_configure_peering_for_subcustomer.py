
"""
Test configuring peering for a subcustomer
"""

from pprint import pprint

import pytest
from ixapi_client.v2.resources import (
    config,
    service,
    crm,
)

from cli.utils.text import print_f, print_err
from ixapi_tests.utils.state import await_state


@pytest.mark.reset
def test_configure_peering(session, args):
    """
    Configure peering
    """
    print_f()
    print_f("<b>configuring peering for subcustomer on exchange lan</b>")

    product_offering = args.exchange_lan_network_service_product_offering_id

    # Management account
    ret, account = crm.accounts_retrieve(session, args.account["id"])
    ret.raise_for_status()

    # Get account and network service to provision
    ret, subaccount = crm.accounts_retrieve(session, args.subaccounts[0]["id"])
    ret.raise_for_status()

    ret, svc = service.network_services_retrieve(
        session, args.exchange_lan_network_service_id)
    ret.raise_for_status()

    # Other required fixtures
    role_assignments = [
        args.subaccounts[0]["implementation_role_assignment_id"],
        args.subaccounts[0]["noc_role_assignment_id"],
    ]
    connection_id = args.subaccounts[0]["connection_id"]

    print_f("<cyan>network service:</cyan> <orange>{}</orange>, ID: {}",
            svc["name"], svc["id"])
    print_f("<cyan>subaccount:</cyan> <orange>{}</orange>, ID: {}",
            subaccount["name"],
            subaccount["id"])

    # Create network service config
    ret, nsc = config.network_service_configs_create(session, {
        "type": "exchange_lan",
        "network_service": svc["id"],
        "role_assignments": role_assignments,
        "connection": connection_id,
        "billing_account": account["id"],
        "managing_account": account["id"],
        "consuming_account": subaccount["id"],
        "product_offering": product_offering,
        "vlan_config": {
            "vlan_type": "dot1q",
            "vlan": 42,
            "vlan_ethertype": "0x8100",
        },
        "listed": False,
    })
    if ret.status_code >= 400:
        print_err(nsc)
        pytest.xfail("could not configure network service")

    print_f("<green>created network service config</green> ID: {}", nsc["id"])
    print_f("<b>Config-State: {}</b>", nsc["state"])

    # Configure required network feature configs
    ret, required_features = service.network_features_list(session, filters={
        "required": True,
        "network_service": svc["id"],
    })
    ret.raise_for_status()

    # Configure network features
    print_f("configure required network features")
    for feature in required_features:
        ret, nfc = config.network_feature_configs_create(session, {
            "type": "route_server",
            "billing_account": account["id"],
            "managing_account": account["id"],
            "consuming_account": subaccount["id"],
            "role_assignments": role_assignments,
            "as_set_v4": "AS-FOO",
            "network_feature": feature["id"],
            "network_service_config": nsc["id"],
            "asn": 9999999,
            "session_mode": "public",
            "bgp_session_type": "active",
            "ip": nsc["ips"][0],
        })
        if ret.status_code >= 400:
            print_err(nfc)
            pytest.xfail("errr")

        print_f("<green>created network feature config:</green> {}", nfc["id"])

    print_f("<grey>awaiting network service config state:</grey> "
            "<b>PRODUCTION</b>")

    nsc = await_state(
        session,
        config.network_service_configs_retrieve,
        nsc["id"],
        "production",
        raise_on_error_state=False)

    print_f("<green>exchange lan access: configured.</green>")
    pprint(nsc)

