
"""
IX-API schema helper
"""

import os

import requests


class SchemaDownloadError(RuntimeError):
    pass


def schema_filename(version):
    return "ix-api-schema-v{}.json".format(version)


def schema_path(version, base="schema"):
    """Derive schema path"""
    return os.path.join(base, schema_filename(version))


def download(version, base="schema"):
    """Download ix api spec version"""
    filename = schema_filename(version)
    url = "https://docs.ix-api.net/v{}/ix-api-latest.json".format(version)
    response = requests.get(url)
    response.raise_for_status() # E.g. 404

    with open(schema_path(version, base), "w") as f:
        f.write(response.text)


def exists(version, base="schema"):
    """Check if the schema file exists"""
    return os.path.exists(schema_path(version, base))


