
"""
Configuration loader
"""

import json
import urllib

import requests


def load_json_config(uri):
    """
    Load configuration from uri or path
    """
    loader = get_loader(uri)
    data = loader(uri)

    return json.loads(data)


def _load_path(path):
    """Load file"""
    with open(path) as f:
        return f.read()


def _load_uri(url):
    """Load uri using request"""
    response = requests.get(url)
    return response.text


def get_loader(uri):
    """Get loader"""
    parsed = urllib.parse.urlparse(uri)
    if parsed.scheme:
       return _load_uri
    return _load_path


