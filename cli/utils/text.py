
"""Text/UI utilities"""

import inspect

from prompt_toolkit.formatted_text import HTML
from prompt_toolkit import print_formatted_text


def print_f(*args, **kwargs):
    """
    Print "HTML" formatted markup. Args and
    kwargs will be passed to format.

    WARNING: This implementation is unsafe.
    """
    try:
        markup = args[0]
        args = args[1:]
    except IndexError:
        markup = ""
    print_formatted_text(
        HTML(markup.format(*args, **kwargs)))


def print_doc(docstring, *args, **kwargs):
    """
    Print a (formatted) docstring
    """
    print_f(inspect.cleandoc(docstring), *args, **kwargs)


def print_err(error):
    """pretty print error message"""
    if not isinstance(error, dict):
        print_f("<red>ERROR:</red><b> {}", error)
        return

    print_f("<red>ERROR:</red><b> {}</b>", error.get("title"))
    print_f("<orange>{}</orange>", error.get("type"))
    for prop in error.get("properties", []):
        print(prop)
    print()
