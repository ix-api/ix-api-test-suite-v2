"""
Launch the test suite shell

Based upon: https://github.com/mhannig/traack/console/

Try to launch ipthon or bpython. Fall back to the
standard python interpreter.
"""
import sys
from argparse import ArgumentParser, Namespace

from cli.shell import bootstrap
from cli.utils.text import print_f, print_doc
from cli.utils.config import load_json_config


def banner(args):
    """Print some pretty welcome banner"""
    print_doc("""
        <b><orange>IX-API Shell</orange></b>

        <b>Authentication Endpoint:</b>\t\t/v1/auth

        <b>Configuration:</b>
          - host: \t\t {api_base_url}
          - api key:  \t\t {api_key}
          - api secret: \t\t *SET*

    """, api_base_url=args.api_base_url, api_key=args.api_key)

def usage_v1():
    """Print the api v1 usage"""
    print_doc("""
        <b>Usage:</b>
          Use the configured <b>`client`</b> to interact with the API:
            - dial() opens a new authorized session

          The shell provides an opened <b>`session`</b>.
          You can access your current customer with `customer`.
    """)
    print()

def parse_args():
    """
    Parse commandline arguments
    """
    parser = ArgumentParser()
    parser.add_argument(
        "-H", "--api-host",
        dest="api_base_url",
        required=False,
        help="Connect to this host")
    parser.add_argument(
        "-k", "--api-key",
        dest="api_key",
        required=False,
        help="Use this api key")
    parser.add_argument(
        "-s", "--api-secret",
        dest="api_secret",
        required=False,
        help="The corresponding api secret")
    parser.add_argument(
        "-C", "--test-config",
        help="A test configuration url")

    args = parser.parse_args()
    # Check if either a test config is present,
    # otherwise use explicit params
    if not args.test_config:
        if not args.api_key or \
            not args.api_secret or \
            not args.host:
                parser.print_usage()
                sys.exit(-1)

    # Load config file if available
    if args.test_config:
        config = load_json_config(args.test_config)
        args = Namespace(**config)

    # Prepare args
    if args.api_base_url.endswith("/"):
        # Remove trailing slash
        args.api_base_url = args.api_base_url[:-1]
    if not args.api_base_url.startswith("http"):
        # Assume http
        args.api_base_url = "http://" + args.api_base_url

    return args


def start_ipython(user_ns={}):
    """Start the ipython shell"""
    from IPython import start_ipython
    start_ipython(argv=[], user_ns=user_ns)


def start_bpython(locals_={}):
    """Start the bpython shell"""
    from bpython import embed
    embed(locals_=locals_)


def start_fallback(local={}):
    """Start the fallback interpreter"""
    from code import interact
    interact(local=local)


def start_shell(local={}):
    """
    Start a python shell
    """
    shells = [start_ipython, start_bpython, start_fallback]
    for shell in shells:
        try:
            shell(local)
        except ImportError:
            pass # try next
        else:
            return


def __main__():
    """
    Start an API console
    """
    args = parse_args()

    # Infomercials
    banner(args)
    usage_v1()

    # Boostrap client
    shell_locals = bootstrap.init(args)

    # Start interactive session
    start_shell(shell_locals)

