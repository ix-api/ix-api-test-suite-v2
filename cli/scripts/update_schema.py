
"""
Download latest schema version
"""

from cli.utils import schema
from cli.utils.text import print_f


def __main__():
    """Download the ix-api schema"""
    print_f("<orange>Downloading ix-api-schema v1...</orange>")
    schema.download(1)
    print_f("<orange>Downloading ix-api-schema v2...</orange>")
    schema.download(2)
    print_f("<green>Schema files updated.</green>")

