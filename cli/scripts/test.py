"""
Launch the testrunner pytest
"""

import subprocess
import sys

def __main__():
    """Run pytest on ixapi_tests"""
    tests = sys.argv[1:]
    if not tests:
        tests = ["ixapi_tests/"]

    result = subprocess.run([
        "pytest",
        "-s", "-v",
        *tests,
    ])

    sys.exit(result.returncode)
