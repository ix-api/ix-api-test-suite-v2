
from secrets import token_hex
from time import sleep

import pytest

from cli.utils.text import print_f, print_err
from ixapi_client.v2.resources import (
    crm,
)

def test_delete_account(session):
    """Delete account"""
    print_f("<b>creating account</b>")
    rnd = token_hex(8)
    ret, account = crm.accounts_create(session, {
        "name": f"TestAccount{rnd}",
        "address": {
            "country": "de",
            "locality": "city",
            "postal_code": "01235",
            "street_address": "streetway 23"
        }
    });

    ret.raise_for_status()

    # Delete account
    ret, res = crm.accounts_destroy(session, account["id"])


