
import sys
from argparse import Namespace

import pytest
import requests

from cli.utils.config import load_json_config
from cli.utils.text import print_f
from ixapi_client.v2 import client

# Global configuration
_ARGS = None
_IX_API_CONFIG = None
_SESSION = None


@pytest.fixture
def session():
    """An authenticated api session"""
    global _SESSION
    if not _SESSION:
        _SESSION = client.dial(
            host=_IX_API_CONFIG.api_base_url,
            api_key=_IX_API_CONFIG.api_key,
            api_secret=_IX_API_CONFIG.api_secret)

    return _SESSION

# Configuration fixture

@pytest.fixture
def args():
    """Get ixapi configuration"""
    return _IX_API_CONFIG


def pytest_runtest_setup(item):
    """Before running a test check if we need to reset the API"""
    if not list(item.iter_markers(name="reset")):
        return
    # Make request
    ret = requests.post(_IX_API_CONFIG.reset_trigger_url)
    if ret.status_code != 200:
        raise Exception("could not reset API")


def pytest_addoption(parser):
    """
    Add configuration options for the api test suite:
     - api key
     - api secret
     - host
     or as an alternative
     - test-config
    """
    group = parser.getgroup("ixapi")
    group.addoption(
        "--api-key",
        action="store",
        dest="api_key",
        required=False,
        default=None,
        help="The key for accessing the api")

    group.addoption(
        "--api-secret",
        action="store",
        dest="api_secret",
        required=False,
        default=None,
        help="The secret for accessing the api")

    group.addoption(
        "--api-host",
        dest="api_base_url",
        required=False,
        default="",
        help="The server where the API is hosted")

    group.addoption(
        "--openapi-spec",
        action="store",
        dest="spec_file",
        default="schema/ix-api-schema-v2.json",
        help="The ix-api v1 OpenAPI spec file")

    group.addoption(
        "--test-config",
        action="store",
        dest="test_config",
        required=False,
        default=None,
        help="A path or uri to a test suite configuration json")


def pytest_configure(config):
    """
    Setup pytest and configure ix-api environment
    """
    args = config.option
    if not args.test_config:
        if not args.api_key or \
            not args.api_secret or \
            not args.host:
            pytest.exit("Missing IX-API configuration", 4)

    global _ARGS
    _ARGS = args

    # Load config file if available
    if args.test_config:
        config = load_json_config(args.test_config)
        args = Namespace(**config)

    # Prepare args
    if args.api_base_url.endswith("/"):
        # Remove trailing slash
        args.api_base_url = args.api_base_url[:-1]
    if not args.api_base_url.startswith("http"):
        # Assume http
        args.api_base_url = "http://" + args.api_base_url

    print_f("")
    print_f("<orange>IX-API TEST SUITE</orange>")
    print("")
    print_f("<b>Configuration:</b>")
    print(" - host: \t\t {}".format(args.api_base_url))
    print(" - api key:  \t\t {}".format(args.api_key))
    print(" - api secret: \t\t *SET*")
    print("")

    global _IX_API_CONFIG
    _IX_API_CONFIG = args

    # Inform our code that we are running in test suite mode
    setattr(sys, "_is_ixapi_test_suite", True)
