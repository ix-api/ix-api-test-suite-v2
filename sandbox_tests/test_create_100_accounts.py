
from secrets import token_hex
from time import sleep

import pytest

from cli.utils.text import print_f, print_err
from ixapi_client.v2.resources import (
    crm,
)

def test_create_100_accounts(session):
    """Create 100 accounts"""
    print_f("<b>creating 100 accounts</b>")
    for i in range(0, 100):
        create_account(session, i)

def create_account(session, num):
    """Create an account"""
    rnd = token_hex(8)
    ret, account = crm.accounts_create(session, {
        "name": f"TestAccount{rnd} {num}",
        "address": {
            "country": "de",
            "locality": "city",
            "postal_code": "01235",
            "street_address": "streetway 23"
        }
    });

    ret.raise_for_status()

    # await_account_production(session, account)


def await_account_production(session, account):
    """Poll until account has reached production or timeout"""
    retries = 0
    max_retries = 10
    print_f("<b>Awaiting state production for: {}</b>", account["name"])
    while True:
        ret, acc = crm.accounts_retrieve(session, account["id"])
        assert ret.ok, ret

        if acc["state"] == "production":
            print_f("account state: <green>{}</green>", acc["state"])
            return

        sleep(0.25)
        retries += 1
        if retries >= max_retries:
            print_f("account state: <red>{}</red>", acc["state"])
            raise("account never reached production")
